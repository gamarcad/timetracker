import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentiction.services';
import { Observable } from 'rxjs';


@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor( private authService: AuthenticationService, private router : Router ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
	if ( this.authService.isAuthenticated() ) {
		return true;
	} else {
		this.router.navigate([ "/login" ], {
			"queryParams": {
				"next_url": route.url,
			}
		})
	}
  }
}