import { User } from '../_entities/user.entity'
import { Injectable } from '@angular/core';
import { DataHolder } from './data_holder';

@Injectable()
export class AuthenticationService {

	private static USER_STORAGE_KEY : string = "user";
	private static AUTHENTICATED_STORAGE_KEY : string = "authenticated";

	private user : User;
	private authenticated : boolean;
	

	constructor(  private dataHolder : DataHolder ) {
		this.retreiveUser()
	}

	public isAuthenticated() : boolean {
		return this.authenticated;
	}

	public authenticatedUser() : User {
		return this.user;
	}

	public login( username : string, password : string ) {
		return new Promise(
			( resolve, reject ) => {
				// retreive user
				const user : User = this.dataHolder.getData().find( 
					( user : User ) => {
						return user.username === username && user.password === password
					}
				)

				if ( user ) {
					this.connectUser( user );
					resolve( user );
				} else {
					reject();
				}
			}
		)
		
	}

	public logout() {
		this.authenticated = false;
		this.authenticatedUser = undefined;
		sessionStorage.clear();
	}

	private connectUser( user : User ) {
		this.authenticated = true;
		this.user = user;
		sessionStorage.setItem( AuthenticationService.USER_STORAGE_KEY, JSON.stringify( user ) );
		sessionStorage.setItem( AuthenticationService.AUTHENTICATED_STORAGE_KEY, "true" );
	}

	private retreiveUser() {
		this.authenticated = sessionStorage.getItem( AuthenticationService.AUTHENTICATED_STORAGE_KEY ) === "true" ? true : false;
		if ( this.authenticated ) {
			this.user = JSON.parse( sessionStorage.getItem( AuthenticationService.USER_STORAGE_KEY )) ;
		}
	}	
}