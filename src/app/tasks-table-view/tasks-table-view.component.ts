import { Component, OnInit, Input, ViewChild, OnDestroy } from "@angular/core";
import { MatTableDataSource, MatPaginator } from "@angular/material";
import { Task } from "../_entities/task.entity";
import { FacadeService } from "../_services/facade.service";
import { Router } from "@angular/router";
import { DurationComputer } from "../_services/duration.service";
import { interval, Subscription } from 'rxjs';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: "app-tasks-table-view",
  templateUrl: "./tasks-table-view.component.html",
  styleUrls: ["./tasks-table-view.component.css"],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TasksTableViewComponent implements OnInit, OnDestroy {
  // tasks table columns
  private tasksTableColumns = [
    "name",
    "creation",
    "duration",
    "status",
    "action",
  ];

  private tasksDataSource: MatTableDataSource<Task> = new MatTableDataSource();
  @ViewChild(MatPaginator, { static: true }) tasksPaginator: MatPaginator;
  private durationByTask: Map<number, String> = new Map();
  private activeTasksOnly: boolean = false;
  private taskFilter: string;
  private durationsUpdateSubscription : Subscription;

  @Input() private tasks: Task[];

  constructor(private facadeService: FacadeService, private router: Router) {}

  ngOnInit() {
    this.paintComponent();
    this.facadeService.subscribeToUpdate((_) => this.paintComponent());

    this.updateTotalDurations()
    this.durationsUpdateSubscription = interval( 1000 ).subscribe( _ => this.updateTotalDurations() )

    // initiate task data source filter predicate
    this.tasksDataSource.filterPredicate = (data: Task, filter: string) => {
      const filteringData: string[] = filter.split("&");
      const filterText = filteringData[0];
      const activeTasks = filteringData[1] === "true" ? true : false;
      console.log(
        "Filter ",
        data,
        " with filter ",
        filter,
        filterText,
        activeTasks
      );
      if (!JSON.stringify(data).match(filterText)) {
        return false;
      }
      if (!this.facadeService.isActiveTask(data) && activeTasks) {
        return false;
      }
      return true;
    };


  }

  ngOnDestroy() {
    this.durationsUpdateSubscription.unsubscribe()
  }

  paintComponent() {
    console.log("Painting components", this.tasks);
    this.tasksDataSource.data = this.tasks;
    this.tasksDataSource.paginator = this.tasksPaginator;
  }

  goToTask(task: Task) {
    return this.router.navigate(["/tasktimer/task/" + task.id]);
  }

  isActiveTask(task: Task) {
    return this.facadeService.isActiveTask(task);
  }

  startTask(task: Task) {
    this.facadeService.startTask(task);
  }

  stopTask(task: Task) {
    this.facadeService.stopTask(task);
  }

  updateTotalDurations() {
    for (let task of this.tasks) {
      this.durationByTask[task.id] = DurationComputer.millisecondsToDuration(
        this.facadeService.getTotalDuration(task)
      );
    }
  }

  //////////////////////////////////////////////////////////////////////////////////
  // TASKS ACTIONS
  //////////////////////////////////////////////////////////////////////////////////
  applyTasksFilter() {
    console.log("Applying tasks filter");
    this.tasksDataSource.filter =
      (this.taskFilter ? this.taskFilter : "") + "&" + this.activeTasksOnly;
    //const filterValue = (event.target as HTMLInputElement).value;
    //this.tasksDataSource.filter = filterValue.trim().toLowerCase();
  }

  toggleActiveTasksFilter() {
    setTimeout((_) => this.applyTasksFilter(), 100);
  }
}
